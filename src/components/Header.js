import React from 'react';

import ContextSearch from './ContextSearch'

const Header = () =>{
    return (
          <header className="header-principal">
            <ul className="menu-principal">
              <li>Pesquisa TVMaze</li>
                <ContextSearch.Consumer>
                  { 
                    (context)=>{
                      return( 
                        <>
                          {
                            context.header !== undefined ?
                            <li style={{'paddingLeft':'15px'}}>{context.header}</li>
                            : 
                            <RenderHeader response={context.data}/>
                          }
                        </>
                      )
                    }               
                  }
                </ContextSearch.Consumer>
            </ul>
          </header>
        )
}

const RenderHeader = ({response}) =>{
  return (
    <>
    {
      response.map((data, index)=>
      <ul key={index} className="headerMovie">
         <li className="titleShow">{data.name}</li>
         <li><img src={data.image !== undefined ? data.image.medium :``} alt={data.name} width="50" /></li>
      </ul>
      )
    }
  </>
  )

}

export default Header;