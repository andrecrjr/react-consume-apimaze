import React, { Fragment, useState, useEffect } from 'react';
import ContextSearch from '../ContextSearch'
import Header from '../Header'
import { Link } from 'react-router-dom';
import { api_single } from '../../service'


const DescriptionRouter = ({match}) => {
    const [show, setShow] = useState({response:[]})
  
  
    useEffect(()=>{
          const requestOneShow = async () =>{
            try{
              const result = await api_single.get(`${match.params.id}`)
              setShow({response:[result.data]})
            }catch{
              setShow({response:{"error":"problema"}})
            }
          }
        requestOneShow()
    }, [match.params.id])
  
    return (
      <Fragment>
        <ContextSearch.Provider value={{data:show.response}}>
            <Header/>
        </ContextSearch.Provider>
        {  
          show.response ?
            show.response.map((result)=>{
                return (<ul key={match.params.id}>
                            <li>{result.name}</li>
                            <li>{result.summary}</li>
                        </ul>)
            }) : <p>Nenhuma informação</p>
        }
        <Link to="/">Voltar</Link>
      </Fragment>
    )
  }
  
export default DescriptionRouter;