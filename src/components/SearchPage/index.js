
import React, { Component, Fragment } from 'react';
import ContextSearch from '../ContextSearch'
import Header from '../Header';
import { Link } from 'react-router-dom';
import api from '../../service'

const INITIAL_STATE = {
    busca: null,
    valor: "",
    results:[],
    error:[]
}

export default class SearchPage extends Component{
    constructor(props){
      super(props)  
      this.state = { ...INITIAL_STATE }
    }
  
    getRequest = async (busca) =>{
      try{
        const response = await api.get(`?q=${busca}`)
        this.setState({results:response.data})     
      }catch{
        this.setState({error:{"error":"problema"}})
      }
    }
  
    componentDidMount(){
      this.getRequest(this.state.valor)
    }
    
    componentDidUpdate(prevProps,prevState){
      if(prevState.valor !==this.state.valor){
        this.getRequest(this.state.valor)
      }
    }
  
    submitForm = (e) =>{
      e.preventDefault()
      this.getRequest(this.state.valor)
    }
  
    render(){
        const { valor, results } = this.state
      return (
        <Fragment>
        <ContextSearch.Provider value={{results:results, header:valor}}>
            <Header/>
                <form onSubmit={this.submitForm} className="search-box">
                <label>Pesquisa tvmaze: </label>
                <input type="text" onChange={(e)=>this.setState({valor:e.target.value})} value={valor} placeholder="default"/>
                <input type="submit" value="Pesquisar"/>
                </form>
            { 
              results.length !== 0 ?
              <ListSearch/> :
              <NoRender/>
            }
        </ContextSearch.Provider>
        </Fragment>
      )
    }
  
  }
  
  
const ListSearch = () =>{
    return (
        <Fragment>
          <section className="root-search">
          <ContextSearch.Consumer>
            {
              (context)=>
                  context.results.map((response)=>
                    <RenderSearch response={response} key={response.show.id}/>
                  )
            }
            </ContextSearch.Consumer>
            </section>
          </Fragment>
      )
  }

const RenderSearch = ({response}) =>{
  return (
      <div className="each-search">
      <ul>
            <li><Link to={`/show/${response.show.id}`}>{response.show.name}</Link></li>
            <li><img src={response.show.image ? response.show.image.medium : ""} 
                  alt={response.show.name}/></li>
      </ul>
    </div>
  )
}

const noRender = {
        'marginTop':'25px', 
        'textAlign':'center',
        'color':'red'
      }

const NoRender = () =>{
  return <section style={noRender}>Nenhum dado informado</section>
}
  
  