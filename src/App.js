import React from 'react';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import SearchBox from './components/SearchPage'
import DescriptionRouter from './components/IndividualPage'


const ContainerApp = () =>{
  
  return (
      <Router>
        <Switch>
          <Route exact path="/" component={SearchBox} />
          <Route path="/show/:id" component={DescriptionRouter}/>
        </Switch> 
      </Router>
    )

}

export default ContainerApp;